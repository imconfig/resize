/*
resize.exe - a console program to resize the JPEG picture to any size

Usage: resize [-src="."] [-dst="new"] [-w=1920] [-h=1080]
	src: the folder of the original JPEG files
	dst: the folder name used to store converted files.
	  w: new width of the picture
	  h: new height of the picture

Example:
	// Convert all JPEG pictures in and under the folder to size 1920x1080,
	// And store the converted files to "TVFormat" folder orignal folder
	> resize

	// Resize the JPEG files in "C:\temp\mypic" and any subfolders
	// store converted files to "newpic" folder under src folder
	// The new picture size is 1024x768
	> resize -src="C:\temp\mypic" -dst="newpic" -w=1024 -h=768
*/
package main

import (
	"flag"
	"fmt"
	"image/jpeg"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/nfnt/resize"
)

var src = flag.String("src", ".", "The folder contains the JPG files. Default: Current folder")
var dst = flag.String("dst", "TVFormat", "Specify the folder name for the converted files")
var w = flag.Uint("w", 1920, "Specify the picture width (Pixels)")
var h = flag.Uint("h", 1080, "Specify the picture height (Pixels)")

func main() {

	flag.Parse()

	s, err := filepath.Abs(*src)
	chk(err)
	// Place all converted files in this folder under *src
	d, err := filepath.Abs(filepath.Join(*src, *dst))
	chk(err)

	fmt.Println("  Version:  1.0")
	fmt.Println("     From: ", s)
	fmt.Println("       To: ", d)
	fmt.Println("File size: ", *w, "X", *h)

	sf, err := os.Open(s)
	chk(err)
	defer sf.Close()

	//fmt.Println("Change to folder: ", s)
	err = os.Chdir(s)
	if err != nil {
		log.Fatal(err)
	}

	fis, err := sf.Readdir(0)
	chk(err)

	fmt.Print("\nCreating folder: ", d, " ...")
	err = os.MkdirAll(d, 0666)
	if err != nil {
		if !os.IsExist(err) {
			log.Fatal(err)
		} else {
			fmt.Println("Already exist.")
		}
	} else {
		fmt.Println("Done.")
	}

	fmt.Println("\nResizing ...\n")
	nfiles := procFIS(fis, d, s)

	if nfiles == 0 {
		fmt.Println("\nSorry, I cannot find any JPEG files to resize.")
	} else {
		fmt.Printf("\nAll Done. Total %d files.\n\n", nfiles)
		fmt.Printf("The TV format files are in \"%s\" folder.", d)
	}

}

//dir: dst dir
//
func procFIS(fis []os.FileInfo, dst string, src string) int {
	nfiles := 0
	for _, fi := range fis {
		if !fi.IsDir() {
			nfiles += resizeMe(filepath.Join(src, fi.Name()), dst)
			continue
		}
		//Skip TVFormat destination folder
		if fi.Name() == filepath.Base(dst) {
			continue
		}
		subsrc := filepath.Join(src, fi.Name())
		subf, err := os.Open(subsrc)
		chk(err)
		subfis, err := subf.Readdir(0)
		chk(err)
		nfiles += procFIS(subfis, dst, subsrc)
	}
	return nfiles
}

// Is this file a JPG/JPEG file?
func isJPG(name string) bool {
	name = strings.ToUpper(name)
	if strings.HasSuffix(name, ".JPG") || strings.HasSuffix(name, ".JPEG") {
		return true
	}
	return false
}
func chk(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// filename: the full path name of the .JPG file
// dst: the new folder to hold the converted files
// return: number resized
func resizeMe(filename string, dst string) int {
	if !isJPG(filename) {
		return 0
	}

	fmt.Print(filename + " ... ")
	// open original picture
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	// resize to width 1920 using Lanczos resampling
	// and preserve aspect ratio
	//m := resize.Resize(1920, 0, img, resize.Lanczos3)
	m := resize.Resize(*w, *h, img, resize.NearestNeighbor)
	// TODO: Change name if already exist in TVFormat folder
	out, err := os.Create(filepath.Join(dst, filepath.Base(filename)))
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	// write new image to file
	jpeg.Encode(out, m, nil)
	fmt.Println("Done.")
	return 1
}

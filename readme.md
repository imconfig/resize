# resize.exe

a console program to resize the JPEG picture to any size

A program used to resize the JPEG PowerPoint presentation picture to fit TV format
```
Usage: resize [-src="."] [-dst="new"] [-w=1920] [-h=1080]
	src: the folder of the original JPEG files
	dst: the folder name used to store converted files.
	  w: new width of the picture
	  h: new height of the picture
```
## Instruction:

1. Open your PowerPoint presentation
2. “File” > “Save As” > Save to USB disk  
	Save as type: JPEG File Interchange Format  
	Save: Every Slide  
3.	Double click “resize.exe” in your USB
4.	The new files will be in the “TVFormat” folder


### For advanced users:

If you know what the file path is, you can run it anywhere from command line like this:  

    `> resize -src="c:\temp\oldimg" -dst="c:\temp\newimg"`

Or with different size like this:  

	`> resize -src="C:\mypic" -dst="newpic" -w=1024 -h=768`